﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Settings : MonoBehaviour
{
   // public static string PREF_VOLUME = ":app:pref:volume:enabled";
    
    public GameObject Volume;

   public void OnPanel()
   {
       Boolean newValue = !Volume.activeSelf;
        Volume.SetActive(newValue);
     // Vibration.SetActive(!Vibration.activeSelf);
    }

    public void OffHint()
    {
        Volume.SetActive(false);
       // Vibration.SetActive(true);
    }



}
