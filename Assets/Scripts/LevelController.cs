﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    int sceneIndex;
    int levelComplete;

    public GameObject g;

    void Start()
    {  
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        levelComplete = PlayerPrefs.GetInt("LevelComplete");
    }

    public void isEndGame()
    {
        if (sceneIndex == 4)
        {
            Invoke("LoadMainMenu",1f);
        }
        else
        {
            if (levelComplete < sceneIndex)
            {
                PlayerPrefs.SetInt("LevelComplete", sceneIndex);
                Invoke("NextLevel", 1f);
            }
        }
    }

   void NextLevel()
    {
        SceneManager.LoadScene(sceneIndex + 1);
    }
}
