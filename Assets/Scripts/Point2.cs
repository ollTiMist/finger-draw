﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class Point2 : MonoBehaviour
{
    [Header("Hand animation")]
    public GameObject animHint;

    [Header("Full picture")]
    public GameObject animal;

    [Header("Underpainted drawing")]
    public GameObject animal_Off;

    [Header("Next level button")]
    public GameObject Next;

    [Header("Hint button")]
    public GameObject Hint;

    [Header("Start particle")]
    public GameObject effect;

    [Header("Finish point")]
    public GameObject point;

    void OnTriggerEnter2D(Collider2D collision)
    {
        animal_Off.SetActive(false);    //Отключение недорисованного рисунка
        animal.SetActive(true); //Появление полного рисунка животного

        Next.SetActive(true); //Появление кнопки прехода на след уровень
        Hint.SetActive(false); // Отключение кнопки посказки
        animHint.SetActive(false); // Отключение анимации руки

        Instantiate(effect, transform.position = (new Vector2(-7, 2)), Quaternion.identity); //Появление эффекта звёздочек

        point.SetActive(false); // отключаем точку чтобы не появлялись эффекты, после завершения уровня

    }
}
