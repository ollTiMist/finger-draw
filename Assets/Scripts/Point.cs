﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class Point : MonoBehaviour
{
    //Скрипт отвечающий за добавление первой точки!
    //Этот скрипт можно добавлять к скольким угодно точкам на сцене
    
    [Header("Next point")]
    public GameObject button1; //Объект точки
   
    void OnTriggerEnter2D(Collider2D collision)
    {
        button1.SetActive(true); //Появление точки
    }


    //Метод при котором точка отключается, если вы не провели по точке
    private void OnTriggerExit2D(Collider2D collision)
    {
        button1.SetActive(false);  //Отключение точки
    }







}
