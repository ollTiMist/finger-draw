﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveLevel : MonoBehaviour
{
    

    public static int levelComplete;
    public GameObject settingsPanel;

    void Start()
    {
        levelComplete = PlayerPrefs.GetInt("LevelComplete", NextLevel.levelIndex);
        DontDestroyOnLoad(gameObject);
    }

    public void Update()
    {
        if (SceneManager.GetActiveScene().name != "Start")
        {
            settingsPanel.SetActive(true);
        }
    }

}
