﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour
{
    public static int levelIndex;
    private void Start()
    {
        levelIndex = SceneManager.GetActiveScene().buildIndex + 1;
    }
    public void nextLevel()
    {
        //Events.instance.Raise(new NextLevelGameEvent());
        PlayerPrefs.SetInt("LevelComplete", levelIndex);
        SceneManager.LoadScene(levelIndex);
    }
 
}
