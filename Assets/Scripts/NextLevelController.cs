﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevelController : MonoBehaviour
{
    int levelIndex;
    private void Start()
    {
    }
    public void nextLevel()
    {
        Events.instance.Raise(new  NextLevelGameEvent());
    }
 
}

public class NextLevelGameEvent: GameEvent
{
    
}
