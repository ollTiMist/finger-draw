﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    int levelCount;
    public void Count()
    {
        if (SaveLevel.levelComplete > 0)
        {
            levelCount = SaveLevel.levelComplete;
            SceneManager.LoadScene(levelCount);
        }
        else
            SceneManager.LoadScene(levelCount + 1);

    }

    private void Start()
    {
        levelCount = SceneManager.GetActiveScene().buildIndex;
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(2f);
        Count();
    }
}
