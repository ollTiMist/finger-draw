﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DrawLine::Update()
extern void DrawLine_Update_m707166BCDD346A55544527D1C963D88CB2C1377B (void);
// 0x00000002 System.Void DrawLine::CreateLine()
extern void DrawLine_CreateLine_m6DFADEA0A555336B585F9D6DC5720C60AE2EF79D (void);
// 0x00000003 System.Void DrawLine::UpdateLine(UnityEngine.Vector2)
extern void DrawLine_UpdateLine_m42DC4AC7FC032167940103E09E993EFBB0FF0B62 (void);
// 0x00000004 System.Void DrawLine::Clears()
extern void DrawLine_Clears_m5658F7EBC341B052401A8C691B5BF4F149179876 (void);
// 0x00000005 System.Void DrawLine::.ctor()
extern void DrawLine__ctor_m8609DDD68BCAF632AA855FF766E69B83D1AD01C4 (void);
// 0x00000006 System.Void GameEvent::.ctor()
extern void GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362 (void);
// 0x00000007 Events Events::get_instance()
extern void Events_get_instance_mDF96B334D61E5993C711D8B83EA39FB02346BD34 (void);
// 0x00000008 System.Void Events::AddListener(Events/EventDelegate`1<T>)
// 0x00000009 System.Void Events::RemoveListener(Events/EventDelegate`1<T>)
// 0x0000000A System.Void Events::Raise(GameEvent)
extern void Events_Raise_m278701B197E0ABF57A10EF38B139E2A88D33529A (void);
// 0x0000000B System.Void Events::.ctor()
extern void Events__ctor_m130E4DED4BED869E2F573522022F010E1260BB3E (void);
// 0x0000000C System.Void Events::.cctor()
extern void Events__cctor_m7F185A8AC20BC714F9131432043A859E27FACBA9 (void);
// 0x0000000D System.Void LevelController::Start()
extern void LevelController_Start_mCB3B00E4CE6C8E9B8F83F888128E409850FCF503 (void);
// 0x0000000E System.Void LevelController::isEndGame()
extern void LevelController_isEndGame_m11379A1FD939BE98CA9E8C754B0B57BDAF72D648 (void);
// 0x0000000F System.Void LevelController::NextLevel()
extern void LevelController_NextLevel_m0E2EDCA3F6CF69540C85AB323C0B3BE775E7D57F (void);
// 0x00000010 System.Void LevelController::.ctor()
extern void LevelController__ctor_m6F327591D3569C9F146532A9055BB9A1BE07D21A (void);
// 0x00000011 System.Void Loading::Count()
extern void Loading_Count_m90CD64EA4487C9A60E989E9983352218E4BDEF4B (void);
// 0x00000012 System.Void Loading::Start()
extern void Loading_Start_m1316F0CDD41DDDAD4416B98FCFF89ED3228ABE42 (void);
// 0x00000013 System.Collections.IEnumerator Loading::LoadScene()
extern void Loading_LoadScene_m36D67A6A591C8C925673A999D407C7587AAF3959 (void);
// 0x00000014 System.Void Loading::.ctor()
extern void Loading__ctor_mE163CCF6BD5A7EDA90F61A352261F32C2DE99FFB (void);
// 0x00000015 System.Void NextLevel::Start()
extern void NextLevel_Start_mB3634F601CE49F6BDE9ACA4C2BA5B136D0336FD1 (void);
// 0x00000016 System.Void NextLevel::nextLevel()
extern void NextLevel_nextLevel_m046CBB55BCF86B5AE760CBCB81D06C1CD23BE23D (void);
// 0x00000017 System.Void NextLevel::.ctor()
extern void NextLevel__ctor_mBA5C68ECBD3D9BA086C1F6BA75E845E89098DF59 (void);
// 0x00000018 System.Void NextLevelController::Start()
extern void NextLevelController_Start_m50583F05ACAE2EA58EEDB1C7A3D3C670E5A22CE7 (void);
// 0x00000019 System.Void NextLevelController::nextLevel()
extern void NextLevelController_nextLevel_m5F4D82D5F7C51B533D6D769E97C4AC10E4479BE7 (void);
// 0x0000001A System.Void NextLevelController::.ctor()
extern void NextLevelController__ctor_mA3EBD8A1E2611FB5F56986C97C4741A83CF29764 (void);
// 0x0000001B System.Void NextLevelGameEvent::.ctor()
extern void NextLevelGameEvent__ctor_mF009ECF30AB3F808FFE0DA3997C106CF6710C047 (void);
// 0x0000001C System.Void Point::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Point_OnTriggerEnter2D_mF77060FBE015D179C4B50ABCFE53F0F14DA03376 (void);
// 0x0000001D System.Void Point::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Point_OnTriggerExit2D_mAF9A38D443B30FB73341DA62EC55F3A33C867AC6 (void);
// 0x0000001E System.Void Point::.ctor()
extern void Point__ctor_m7423A417FF831176F417E85141B0A56B11051913 (void);
// 0x0000001F System.Void Point1::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Point1_OnTriggerEnter2D_mFA7988ED82A5C7336C6E71CABE1FD41E18DD4960 (void);
// 0x00000020 System.Void Point1::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Point1_OnTriggerExit2D_m5B80FF34C202DB19888ACAE2E2E26F5FD9C71AD6 (void);
// 0x00000021 System.Void Point1::.ctor()
extern void Point1__ctor_m85335C7B3E528EE06E25092382E120188DD7FC86 (void);
// 0x00000022 System.Void Point2::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Point2_OnTriggerEnter2D_mCEA252D5D6253EE009AA78C022E64FBCC21FAE0E (void);
// 0x00000023 System.Void Point2::.ctor()
extern void Point2__ctor_m0CA0D29650FA33B91521BD7DE8B415008520C547 (void);
// 0x00000024 System.Void SaveLevel::Start()
extern void SaveLevel_Start_m276A0F0392518A18C28630FF1B04E09F37FD9BC4 (void);
// 0x00000025 System.Void SaveLevel::Update()
extern void SaveLevel_Update_m82A6448AE4BB813B5F3F4F329D55CA015F74D0D8 (void);
// 0x00000026 System.Void SaveLevel::.ctor()
extern void SaveLevel__ctor_mE43DFF5F8558A493171F789AD52E8B6D5BD7E9BE (void);
// 0x00000027 System.Void Settings::OnPanel()
extern void Settings_OnPanel_mB006823407A98DBEEB431070CB44E97D34710B23 (void);
// 0x00000028 System.Void Settings::OffHint()
extern void Settings_OffHint_m5E8DF6805AB4EECE722EC173CD2334E612AC623A (void);
// 0x00000029 System.Void Settings::.ctor()
extern void Settings__ctor_m2E00E4F8E213532A3051E24E75CE65368C0BB661 (void);
// 0x0000002A System.Void Events/EventDelegate`1::.ctor(System.Object,System.IntPtr)
// 0x0000002B System.Void Events/EventDelegate`1::Invoke(T)
// 0x0000002C System.IAsyncResult Events/EventDelegate`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x0000002D System.Void Events/EventDelegate`1::EndInvoke(System.IAsyncResult)
// 0x0000002E System.Void Loading/<LoadScene>d__3::.ctor(System.Int32)
extern void U3CLoadSceneU3Ed__3__ctor_m97409A4453E6CFB6815AD8FC42199BED7B414DE9 (void);
// 0x0000002F System.Void Loading/<LoadScene>d__3::System.IDisposable.Dispose()
extern void U3CLoadSceneU3Ed__3_System_IDisposable_Dispose_m0AEE9262E2D41ED2767855F1967A2007513310EF (void);
// 0x00000030 System.Boolean Loading/<LoadScene>d__3::MoveNext()
extern void U3CLoadSceneU3Ed__3_MoveNext_m8FEBCE8E670C75B5BDE6407F642B0290083F48EF (void);
// 0x00000031 System.Object Loading/<LoadScene>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadSceneU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEEB082F35BA5213A2354C3B7ECB17DC6A9EC696F (void);
// 0x00000032 System.Void Loading/<LoadScene>d__3::System.Collections.IEnumerator.Reset()
extern void U3CLoadSceneU3Ed__3_System_Collections_IEnumerator_Reset_m23318832B4917262BD8B2F273A982E0A29BF986D (void);
// 0x00000033 System.Object Loading/<LoadScene>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CLoadSceneU3Ed__3_System_Collections_IEnumerator_get_Current_m2CF9B818FD577116882B9F945A270FDBF37151A6 (void);
static Il2CppMethodPointer s_methodPointers[51] = 
{
	DrawLine_Update_m707166BCDD346A55544527D1C963D88CB2C1377B,
	DrawLine_CreateLine_m6DFADEA0A555336B585F9D6DC5720C60AE2EF79D,
	DrawLine_UpdateLine_m42DC4AC7FC032167940103E09E993EFBB0FF0B62,
	DrawLine_Clears_m5658F7EBC341B052401A8C691B5BF4F149179876,
	DrawLine__ctor_m8609DDD68BCAF632AA855FF766E69B83D1AD01C4,
	GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362,
	Events_get_instance_mDF96B334D61E5993C711D8B83EA39FB02346BD34,
	NULL,
	NULL,
	Events_Raise_m278701B197E0ABF57A10EF38B139E2A88D33529A,
	Events__ctor_m130E4DED4BED869E2F573522022F010E1260BB3E,
	Events__cctor_m7F185A8AC20BC714F9131432043A859E27FACBA9,
	LevelController_Start_mCB3B00E4CE6C8E9B8F83F888128E409850FCF503,
	LevelController_isEndGame_m11379A1FD939BE98CA9E8C754B0B57BDAF72D648,
	LevelController_NextLevel_m0E2EDCA3F6CF69540C85AB323C0B3BE775E7D57F,
	LevelController__ctor_m6F327591D3569C9F146532A9055BB9A1BE07D21A,
	Loading_Count_m90CD64EA4487C9A60E989E9983352218E4BDEF4B,
	Loading_Start_m1316F0CDD41DDDAD4416B98FCFF89ED3228ABE42,
	Loading_LoadScene_m36D67A6A591C8C925673A999D407C7587AAF3959,
	Loading__ctor_mE163CCF6BD5A7EDA90F61A352261F32C2DE99FFB,
	NextLevel_Start_mB3634F601CE49F6BDE9ACA4C2BA5B136D0336FD1,
	NextLevel_nextLevel_m046CBB55BCF86B5AE760CBCB81D06C1CD23BE23D,
	NextLevel__ctor_mBA5C68ECBD3D9BA086C1F6BA75E845E89098DF59,
	NextLevelController_Start_m50583F05ACAE2EA58EEDB1C7A3D3C670E5A22CE7,
	NextLevelController_nextLevel_m5F4D82D5F7C51B533D6D769E97C4AC10E4479BE7,
	NextLevelController__ctor_mA3EBD8A1E2611FB5F56986C97C4741A83CF29764,
	NextLevelGameEvent__ctor_mF009ECF30AB3F808FFE0DA3997C106CF6710C047,
	Point_OnTriggerEnter2D_mF77060FBE015D179C4B50ABCFE53F0F14DA03376,
	Point_OnTriggerExit2D_mAF9A38D443B30FB73341DA62EC55F3A33C867AC6,
	Point__ctor_m7423A417FF831176F417E85141B0A56B11051913,
	Point1_OnTriggerEnter2D_mFA7988ED82A5C7336C6E71CABE1FD41E18DD4960,
	Point1_OnTriggerExit2D_m5B80FF34C202DB19888ACAE2E2E26F5FD9C71AD6,
	Point1__ctor_m85335C7B3E528EE06E25092382E120188DD7FC86,
	Point2_OnTriggerEnter2D_mCEA252D5D6253EE009AA78C022E64FBCC21FAE0E,
	Point2__ctor_m0CA0D29650FA33B91521BD7DE8B415008520C547,
	SaveLevel_Start_m276A0F0392518A18C28630FF1B04E09F37FD9BC4,
	SaveLevel_Update_m82A6448AE4BB813B5F3F4F329D55CA015F74D0D8,
	SaveLevel__ctor_mE43DFF5F8558A493171F789AD52E8B6D5BD7E9BE,
	Settings_OnPanel_mB006823407A98DBEEB431070CB44E97D34710B23,
	Settings_OffHint_m5E8DF6805AB4EECE722EC173CD2334E612AC623A,
	Settings__ctor_m2E00E4F8E213532A3051E24E75CE65368C0BB661,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CLoadSceneU3Ed__3__ctor_m97409A4453E6CFB6815AD8FC42199BED7B414DE9,
	U3CLoadSceneU3Ed__3_System_IDisposable_Dispose_m0AEE9262E2D41ED2767855F1967A2007513310EF,
	U3CLoadSceneU3Ed__3_MoveNext_m8FEBCE8E670C75B5BDE6407F642B0290083F48EF,
	U3CLoadSceneU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEEB082F35BA5213A2354C3B7ECB17DC6A9EC696F,
	U3CLoadSceneU3Ed__3_System_Collections_IEnumerator_Reset_m23318832B4917262BD8B2F273A982E0A29BF986D,
	U3CLoadSceneU3Ed__3_System_Collections_IEnumerator_get_Current_m2CF9B818FD577116882B9F945A270FDBF37151A6,
};
static const int32_t s_InvokerIndices[51] = 
{
	1121,
	1121,
	985,
	1121,
	1121,
	1121,
	1913,
	-1,
	-1,
	960,
	1121,
	1927,
	1121,
	1121,
	1121,
	1121,
	1121,
	1121,
	1090,
	1121,
	1121,
	1121,
	1121,
	1121,
	1121,
	1121,
	1121,
	960,
	960,
	1121,
	960,
	960,
	1121,
	960,
	1121,
	1121,
	1121,
	1121,
	1121,
	1121,
	1121,
	-1,
	-1,
	-1,
	-1,
	951,
	1121,
	1109,
	1090,
	1121,
	1090,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000008, { 0, 1 } },
	{ 0x06000009, { 1, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)1, 94 },
	{ (Il2CppRGCTXDataType)1, 95 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	51,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
